package menu;

import controller.*;
import model.*;

import javax.swing.*;

public class Menu extends JFrame{
    public Menu(JFrame game, Model model, Controller controller) {
        JMenuBar menuBar = new JMenuBar();

        game.setJMenuBar(menuBar);
        JMenu editMenu = new JMenu("Menu");
        menuBar.add(editMenu);
        JMenuItem saveItem = new JMenuItem("Save");
        editMenu.add(saveItem);
        JMenuItem loadItem = new JMenuItem("Load");
        editMenu.add(loadItem);
        JMenuItem ExitItem = new JMenuItem("Exit");
        editMenu.add(ExitItem);

        saveItem.addActionListener(e -> {
            model.saveConfig();
        });

        loadItem.addActionListener(e -> {
            model.loadConfig();
            controller.getView().repaint();
        });

        ExitItem.addActionListener(e -> {
            System.exit(0);
        });

    }

}
