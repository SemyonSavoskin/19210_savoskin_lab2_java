import org.junit.Test;
import services.ReadService;

public class ReadServiceTest {
    @Test
    public void readNextLine(){
        String[] args = {"src/test/java/testCodeFiles/test1.txt"};
        System.out.println(args[0]);
        ReadService readService = new ReadService(args);

        assert ("PUSH 14".equals(readService.readNextLine()));
        assert ("PUSH 4".equals(readService.readNextLine()));
        assert ("-".equals(readService.readNextLine()));
        assert ("PRINT".equals(readService.readNextLine()));


    }
}
