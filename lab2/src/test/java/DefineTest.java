import commands.CommandException;
import commands.Define;
import org.junit.Test;
import parser.CommandAndArgs;
import storage.Storage;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DefineTest {
    @Test
    public void addTestOk() throws CommandException {
        Define def = new Define();
        CommandAndArgs ComAndArg = new CommandAndArgs();
        Storage storage = new Storage();

        ComAndArg.args.add("a");
        ComAndArg.args.add("4");
        def.execute(ComAndArg.args, storage);
        assertEquals (storage.getValue("a"), 4);

        CommandAndArgs CommAndArg1 = new CommandAndArgs();
        CommAndArg1.args.add("bc");
        CommAndArg1.args.add("14");
        def.execute(CommAndArg1.args, storage);
        assertEquals (storage.getValue("a"), 4);
        assertEquals (storage.getValue("bc"), 14);

    }
}

