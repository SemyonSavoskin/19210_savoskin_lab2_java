import commands.Pop;
import org.junit.Test;
import storage.Storage;

import java.util.EmptyStackException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class PopTest {
    @Test
    public void PopTestOk(){
        Pop pop = new Pop();
        Storage storage = new Storage();

        storage.pushStack(14);
        storage.pushStack(24);
        storage.pushStack(34);
        pop.execute(null, storage);
        assertEquals (storage.peekStack(), 24);
        pop.execute(null, storage);
        assertEquals (storage.peekStack(), 14);
    }
    @Test
    public void popTestNotGood(){
        Pop pop = new Pop();
        Storage storage = new Storage();
        assertThrows(EmptyStackException.class, () -> pop.execute(null, storage));

    }
}

