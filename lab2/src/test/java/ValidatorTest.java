import org.junit.Test;
import parser.CommandAndArgs;
import services.*;
import storage.Storage;
import storage.iStorage;

import java.util.LinkedList;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class ValidatorTest {
    @Test
    public void isValid() throws ValidatorException {

        CommandAndArgs commandAndArgs = new CommandAndArgs();
        commandAndArgs.args = new LinkedList<>();
        iStorage storage = new Storage();

        // Bad situations
        // 1) Команды не существует
        String[] badCommands = {"", "bad", "\n", "PU SH", "1", "1\n"};
        for (String badCommand : badCommands) {
            commandAndArgs.commandName = badCommand;
            assertThrows(ValidatorException.class, () -> Validator.isValid(commandAndArgs, storage));
        }

        // 2) Неверное число аргументов
        commandAndArgs.commandName = "PUSH";
        try {
            Validator.isValid(commandAndArgs, storage);
        } catch (ValidatorException ex){
            assert ("For command \"PUSH\" expected 1 arguments, detected 0".equals(ex.getMessage()));
        }
        commandAndArgs.args.add("1");
        commandAndArgs.args.add("2");
        try {
            Validator.isValid(commandAndArgs, storage);
        } catch (ValidatorException ex){
            assert ("For command \"PUSH\" expected 1 arguments, detected 2".equals(ex.getMessage()));
        }
        commandAndArgs.args.clear();
        commandAndArgs.commandName = "POP";
        commandAndArgs.args.add("1");
        try {
            Validator.isValid(commandAndArgs, storage);
        } catch (ValidatorException ex){
            assert ("For command \"POP\" expected 0 arguments, detected 1".equals(ex.getMessage()));
        }
        commandAndArgs.args.clear();
        // 3) На стеке недостаточно аргументов
        commandAndArgs.commandName = "POP";
        try {
            Validator.isValid(commandAndArgs, storage);
        } catch (ValidatorException ex){
            assert ("For command \"POP\" expected 1 elements on stack, but stack contains 0".equals(ex.getMessage()));
        }
        commandAndArgs.args.clear();
        // Все в порядке
        commandAndArgs.commandName = "PUSH";
        commandAndArgs.args.add("4");
        assert (Validator.isValid(commandAndArgs, storage));
        commandAndArgs.args.clear();
        commandAndArgs.commandName = "POP";
        storage.pushStack(1);
        assert (Validator.isValid(commandAndArgs, storage));
        storage.PutToDefineList("b", 5);
        commandAndArgs.commandName = "PUSH";
        commandAndArgs.args.add("b");
        assert (Validator.isValid(commandAndArgs, storage));


    }
}
