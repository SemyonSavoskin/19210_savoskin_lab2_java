import commands.CommandException;
import commands.Mul;
import org.junit.Test;
import storage.Storage;

import java.util.EmptyStackException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class MulTest {
    @Test
    public void mulTestOk() throws CommandException {
        Mul mul = new Mul();
        Storage storage = new Storage();

        storage.pushStack(5);
        storage.pushStack(15);
        mul.execute(null, storage);

        assertEquals (storage.peekStack(), 75);

    }

    @Test
    public void mulTestNotGood(){
        Mul mul = new Mul();
        Storage storage = new Storage();
        assertThrows(EmptyStackException.class, () -> mul.execute(null, storage));

    }
}