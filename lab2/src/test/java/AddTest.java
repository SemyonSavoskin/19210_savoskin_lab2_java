import commands.CommandException;
import commands.Add;
import org.junit.Test;
import storage.Storage;

import java.util.EmptyStackException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class AddTest {
    @Test
    public void AddTestOk() throws CommandException {
        Add add = new Add();
        Storage storage = new Storage();

        storage.pushStack(5);
        storage.pushStack(15);
        add.execute(null, storage);

        assertEquals (storage.peekStack(), 20);

    }

    @Test
    public void addTestNotGood(){
        Add add = new Add();
        Storage storage = new Storage();
        assertThrows(EmptyStackException.class, () -> add.execute(null, storage));

    }
}