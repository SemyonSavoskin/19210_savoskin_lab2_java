import commands.CommandException;
import commands.Div;
import org.junit.Test;
import storage.Storage;

import java.util.EmptyStackException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class DivTest {
    @Test
    public void divTestOk() throws CommandException {
        Div div = new Div();
        Storage storage = new Storage();

        storage.pushStack(5);
        storage.pushStack(15);
        div.execute(null, storage);

        assertEquals (storage.peekStack(), 3);

    }

    @Test
    public void divTestNotGood(){
        Div div = new Div();
        Storage storage = new Storage();
        assertThrows(EmptyStackException.class, () -> div.execute(null, storage));

    }
}