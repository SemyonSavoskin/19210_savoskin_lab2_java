import org.junit.Test;
import parser.CalcParser;
import parser.CommandAndArgs;


public class ParserTest {
    @Test
    public void parse(){
        // correct strings
        
        CommandAndArgs commandAndArgs = CalcParser.parse("PUSH 5");
        assert ("PUSH".equals(commandAndArgs.commandName));
        assert (commandAndArgs.args.size() == 1);
        assert ("5".equals(commandAndArgs.args.get(0)));

        commandAndArgs = CalcParser.parse("POP");
        assert ("POP".equals(commandAndArgs.commandName));
        assert (commandAndArgs.args.size() == 0);

        commandAndArgs = CalcParser.parse("PUSH b");
        assert ("PUSH".equals(commandAndArgs.commandName));
        assert (commandAndArgs.args.size() == 1);
        assert ("b".equals(commandAndArgs.args.get(0)));

        commandAndArgs = CalcParser.parse("SQRT 25");
        assert ("SQRT".equals(commandAndArgs.commandName));
        assert (commandAndArgs.args.size() == 1);
        assert ("25".equals(commandAndArgs.args.get(0)));

        commandAndArgs = CalcParser.parse("PRINT a");
        assert ("PRINT".equals(commandAndArgs.commandName));
        assert (commandAndArgs.args.size() == 1);
        assert ("a".equals(commandAndArgs.args.get(0)));

        commandAndArgs = CalcParser.parse("DEFINE a 5");
        assert ("DEFINE".equals(commandAndArgs.commandName));
        assert (commandAndArgs.args.size() == 2);
        assert ("a".equals(commandAndArgs.args.get(0)));
        assert ("5".equals(commandAndArgs.args.get(1)));

        commandAndArgs = CalcParser.parse("DEFINE a1 5");
        assert ("DEFINE".equals(commandAndArgs.commandName));
        assert (commandAndArgs.args.size() == 2);
        assert ("a1".equals(commandAndArgs.args.get(0)));
        assert ("5".equals(commandAndArgs.args.get(1)));

        commandAndArgs = CalcParser.parse("PUSH qwertyuiop");
        assert ("PUSH".equals(commandAndArgs.commandName));
        assert (commandAndArgs.args.size() == 1);
        assert ("qwertyuiop".equals(commandAndArgs.args.get(0)));

        // comment
        commandAndArgs = CalcParser.parse("#\n");
        assert (commandAndArgs == null);
        commandAndArgs = CalcParser.parse("# bdfb");
        assert (commandAndArgs == null);
        commandAndArgs = CalcParser.parse("#dsberheh");
        assert (commandAndArgs == null);


    }

}
