import commands.CommandException;
import commands.Push;
import org.junit.Test;
import parser.CommandAndArgs;
import storage.Storage;

import java.util.EmptyStackException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class PushTest {
    @Test
    public void pushTestOk() throws CommandException {
        Push push = new Push();
        CommandAndArgs ComAndArg = new CommandAndArgs();
        Storage storage = new Storage();

        ComAndArg.args.add("24");
        push.execute(ComAndArg.args, storage);
        assertEquals (storage.peekStack(), 24);

        CommandAndArgs ComAndArg1 = new CommandAndArgs();
        ComAndArg1.args.add("134");
        push.execute(ComAndArg1.args, storage);
        assertEquals (storage.peekStack(), 134);
    }
    public void pushTestNotGood(){
        Push push = new Push();
        CommandAndArgs ComAndArg = new CommandAndArgs();
        Storage storage = new Storage();
        ComAndArg.args.add("qwer");
        assertThrows(EmptyStackException.class, () -> push.execute(ComAndArg.args, storage));

    }

}

