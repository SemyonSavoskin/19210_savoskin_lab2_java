import commands.Factory;
import commands.iCommand;
import org.junit.Test;

public class FactoryTest {
    @Test
    public void create(){
        Factory factory = new Factory();

        iCommand command = factory.create("PUSH");
        assert ("class commands.Push".equals(command.getClass().toString()));
        command = factory.create("POP");
        assert ("class commands.Pop".equals(command.getClass().toString()));
        command = factory.create("PRINT");
        assert ("class commands.Print".equals(command.getClass().toString()));



    }
}
