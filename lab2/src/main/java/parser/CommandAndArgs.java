package parser;

import java.util.LinkedList;

public class CommandAndArgs {
    public CommandAndArgs(){
        args = new LinkedList<>();
        commandName = null;
    }
    public String commandName;
    public LinkedList<String> args;
}
