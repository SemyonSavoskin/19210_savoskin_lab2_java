package parser;
public class CalcParser {

    public static CommandAndArgs parse(String string)  {

        if (!string.isEmpty() && "#".equals(Character.toString(string.charAt(0)))) return null;

        String[] words =string.split(" ");
        CommandAndArgs commandAndArgs = new CommandAndArgs();
        commandAndArgs.commandName=words[0];
        for (int i = 1; i < words.length; i++){
            try{
                commandAndArgs.args.add(words[i]);
            }
            catch (UnsupportedOperationException e){
                System.err.println("Add operation is not supported by this list " + e.getMessage());
            }

        }

        return commandAndArgs;
    }

}
