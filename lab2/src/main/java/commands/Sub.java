package commands;

import storage.iStorage;

import java.util.List;

public class Sub implements iCommand{
    @Override
    public void execute(List<String> arguments, iStorage storage) throws CommandException {
        storage.pushStack (storage.popStack() - storage.popStack());
    }
}
