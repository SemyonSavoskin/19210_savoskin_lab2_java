package commands;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Properties;

public class Factory {
    private final HashMap<String, String> commandNameToFullName = new HashMap<>();

    public Factory() {
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream("src/main/java/commands/calc.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (String commandShortName : properties.stringPropertyNames()){
            String[] propirties = (properties.getProperty(commandShortName).
                                     split("\\|"));
            String commandFullName = propirties[0];
            commandNameToFullName.put(commandShortName, commandFullName);
        }

    }

    public iCommand create(String command) {
        try {
            return (iCommand)Class.forName(commandNameToFullName.get(command)).
                    getDeclaredConstructor().newInstance();
        } catch (Exception ex){
            ex.printStackTrace();
        }
        return null;
    };
}
