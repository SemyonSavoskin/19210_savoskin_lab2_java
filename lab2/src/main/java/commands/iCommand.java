package commands;

import storage.iStorage;

import java.util.List;

public interface iCommand {
    void execute(List<String> arguments, iStorage storage) throws CommandException;
}
