package commands;

import storage.iStorage;

import java.util.List;

public class Define implements iCommand{

    @Override
    public void execute(List<String> arguments, iStorage storage) throws CommandException {
        String arg1 = arguments.get(0); // слово
        String arg0 = arguments.get(1); // число

        // проверка arg0 на число
        try {
            Integer.parseInt(arg0);          // проверка на int
        } catch (NumberFormatException e) {
            try {
                Double.parseDouble(arg0);    // проверка на double
            } catch (NumberFormatException e1) {
                // если мы тут, значит это слово => исключение
                throw new CommandException("Argument \"" + arg0 +
                        "\" has to be a number");
            }
        }
        // проверка arg1 на слово
        try {
            Integer.parseInt(arg1);          // проверка на int
            throw new CommandException("Argument \"" + arg1 +
                    "\" has to be a word");
        } catch (NumberFormatException e) {
            try {
                Double.parseDouble(arg1);    // проверка на double
                throw new CommandException("Argument \"" + arg1 +
                        "\" has to be a word");
            } catch (NumberFormatException e1) {
                // если мы тут, значит это слово => все в порядке
            }
        }
        if (storage.isDefined(arg1)) System.out.println("Redefined " + arg1);
        storage.PutToDefineList(arg1, Double.parseDouble(arguments.get(1)));
    }
}
