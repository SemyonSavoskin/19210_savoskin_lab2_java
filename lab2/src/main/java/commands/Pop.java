package commands;

import storage.iStorage;

import java.util.List;

public class Pop implements iCommand{

    @Override
    public void execute(List<String> arguments, iStorage storage) {
        storage.popStack();
    }
}
