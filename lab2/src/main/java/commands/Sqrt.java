package commands;

import storage.iStorage;

import java.util.List;

import static java.lang.Math.sqrt;

public class Sqrt implements iCommand{
    @Override
    public void execute(List<String> arguments, iStorage storage) throws CommandException {
        if (storage.peekStack() < 0) throw new CommandException("<0 sqrt");
        storage.pushStack(Math.sqrt(storage.popStack()));
    }


}
