package commands;

import storage.iStorage;

import java.util.List;

public class Div implements iCommand{

    @Override
    public void execute(List<String> arguments, iStorage storage) throws CommandException {
        double temp = storage.popStack();
        if (storage.peekStack() == 0){
            storage.pushStack(temp);
            throw new CommandException("Zero division, stack state didn't change");
        }
        storage.pushStack(temp);
        storage.pushStack (storage.popStack() / storage.popStack());
    }
}
