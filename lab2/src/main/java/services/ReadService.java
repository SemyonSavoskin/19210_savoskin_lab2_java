package services;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class ReadService {
    private ReadType readType;
    private BufferedReader br;
    public ReadService(String[] args){
        if (args.length == 0){
            readType = ReadType.CMD;
        }
        else {
            if (args.length > 1){
                System.out.print("Arguments ignored: ");
                for (int i = 1; i < args.length; i++) {
                    System.out.print(args[i] + " ");
                    if (i > 5) {
                        System.out.print("...");
                        break;
                    }
                }
                System.out.println();
            }
            Path path = Paths.get(args[0]);
            if (!Files.exists(path) || !Files.isReadable(path)){
                System.out.println("File in path \"" + path + "\" doesn't exist or non-readable");
                System.out.println("Ignore this path and waiting input from cmd:");
                readType = ReadType.CMD;
            }else {
                try {
                    this.br = Files.newBufferedReader(path, StandardCharsets.UTF_8);
                    readType = ReadType.FILE;
                }
                catch (IOException exception){
                    System.err.println("Can't open file: " + exception.getMessage());
                }
            }
        }
    }
    public String readNextLine(){
        if (readType == ReadType.FILE){
            try{
                String str;
                if ((str = br.readLine()) != null){
                    return str;
                } else return null;
            } catch (IOException exception){
                System.err.println("Can't read file: " + exception.getMessage());
                return null;
            }
        } else if (readType == ReadType.CMD){
            Scanner in = new Scanner(System.in);
            return in.nextLine();
        } else {
            System.err.println("Unknown ReadType in ReadService");
            return null;
        }
    }
    private enum ReadType {CMD, FILE}
}
