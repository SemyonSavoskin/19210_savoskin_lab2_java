package services;

import parser.CommandAndArgs;
import storage.iStorage;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public final class Validator {
    public static boolean isValid(CommandAndArgs commandAndArgs, iStorage storage) throws ValidatorException  {
        String configPath = "src/main/java/commands/calc.properties";
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream(configPath));
        }catch (IOException ex){
            ex.printStackTrace();
        }
        // 1) Команда существует
        if (!properties.containsKey(commandAndArgs.commandName)){
            throw new ValidatorException("Command \"" + commandAndArgs.commandName + "\" doesn't exist");
        }
        String[] parameters = properties.getProperty(commandAndArgs.commandName).split("\\|");
        // 2) Верное число аргументов
        int expectedCountOfArgs = Integer.parseInt(parameters[1]);
        if (expectedCountOfArgs != commandAndArgs.args.size()){
            throw new ValidatorException("For command \"" + commandAndArgs.commandName +
                    "\" expected " + expectedCountOfArgs + " arguments, detected " + commandAndArgs.args.size());
        }

        // 3) На стеке недостаточно аргументов
        int expectedCountOfStackElements = Integer.parseInt(parameters[2]);
        if (storage.getStackSize() < expectedCountOfStackElements){
            throw new ValidatorExceptionNoArguments("Error For command \"" + commandAndArgs.commandName +
                    "\" expected " + expectedCountOfStackElements +
                    " elements on stack, but stack contains " + storage.getStackSize());
        }

        return true;
    }

}
