package services;

public class ValidatorException extends Exception{
    String message;
    public ValidatorException(String str){
        message = str;
    }

    public ValidatorException() { }
    @Override
    public String getMessage() { return message; }
}
