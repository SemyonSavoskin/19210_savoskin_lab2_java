package storage;

import java.util.EmptyStackException;

public interface iStorage {
    void    pushStack(double number);
    double  popStack() throws EmptyStackException;
    double  peekStack() throws EmptyStackException;
    int     getStackSize();
    void PutToDefineList(String word, double number);
    double getValue(String word);
    boolean isDefined(String word);
}
