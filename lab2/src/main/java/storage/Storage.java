package storage;

import java.util.*;

public final class Storage implements iStorage {
    @Override
    public final void pushStack(double number){
        stack.push(number);
    }
    @Override
    public final double popStack() throws EmptyStackException {
        return stack.pop();
    }
    @Override
    public final double peekStack() throws EmptyStackException {
        return stack.peek();
    }
    @Override
    public int getStackSize() {
        return stack.size();
    }
    @Override
    public final void PutToDefineList(String word, double number) {
        definesList.put(word, number);
    }
    @Override
    public final double getValue(String word){
        return definesList.get(word);
    }
    @Override
    public boolean isDefined(String word) {
        return definesList.containsKey(word);
    }

    public Storage(){
        this.stack = new Stack<>();
        this.definesList = new HashMap<String, Double>();
    }
    private final Stack<Double> stack;
    private final HashMap<String, Double> definesList;
}
