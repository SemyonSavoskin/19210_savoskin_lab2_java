import commands.Factory;
import parser.CalcParser;
import parser.CommandAndArgs;
import services.*;
import storage.Storage;
import storage.iStorage;

public final class Main {
    public static void main(String[] args) {
        ReadService readService = new ReadService(args);
        String line;
        iStorage storage = new Storage();
        Factory factory = new Factory();
        while ((line = readService.readNextLine()) != null){
            try{
                CommandAndArgs commandAndArgs;
                if ((commandAndArgs = CalcParser.parse(line)) == null){
                    continue;
                }
                Validator.isValid(commandAndArgs, storage);
                factory.create(commandAndArgs.commandName).execute(commandAndArgs.args, storage);
            }
            catch (Exception ex){
                System.out.println(ex.getMessage());
            }
        }
    }
}


//C:\Users\Senya\IdeaProjects\lab2\src\main\java\in.txt