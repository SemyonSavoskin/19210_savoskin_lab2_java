package factory.runnables;

import factory.Factory;
import factory.Storage;
import factory.objects.Car;

import java.util.concurrent.TimeUnit;

public class GetCar implements Runnable {
    // запросы дилеров попадают в очередь запросов
    // которую чекает контроллер
    private final Storage<Car> carStorage;

    public GetCar(Storage<Car> carStorage) {
        this.carStorage = carStorage;
    }

    @Override
    public void run() {
        while (true) {
            try {
                TimeUnit.SECONDS.sleep(Factory.dynamicSettings.CAR_DEALER_TIME().getSeconds());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            synchronized (carStorage) {
                Car car = carStorage.get();
                Factory.logger.info("Получение: Dealer {}: Auto {} (Body: {} Motor: {} Accessory: {}) ",
                        Thread.currentThread().getId(), car.CID, car.body.BID, car.motor.MID, car.accessories.AID);
                carStorage.notify();
            }
        }
    }

}
