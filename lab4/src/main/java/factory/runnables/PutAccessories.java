package factory.runnables;

import factory.Factory;
import factory.Storage;
import factory.objects.Accessories;

import java.util.concurrent.TimeUnit;

public class PutAccessories implements Runnable {
    Storage<Accessories> accessoriesStorage;
    public PutAccessories(Storage<Accessories> accessoriesStorage){
        this.accessoriesStorage = accessoriesStorage;
    }
    @Override
    public void run() {
        while (true){
            try {
                TimeUnit.SECONDS.sleep(Factory.dynamicSettings.ACCESSORIES_DEALER_TIME().getSeconds());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            // System.out.println("Положил в склад аксессуаров");
            accessoriesStorage.put(new Accessories((int) (Math.random() * 1000)));
        }
    }
}
