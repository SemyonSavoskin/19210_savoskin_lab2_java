package factory.runnables;

import factory.Storage;
import factory.objects.Car;

public class Controller implements Runnable {
    // просыпается, когда со склада готовой продукции что-то отгрузили
    // анализирует склад, если там есть место, то отправляет воркерам сигнал
    private final Storage<Car> carStorage;
    private final Detector detector;

    public Controller(Storage<Car> carStorage, Detector detector) {
        this.carStorage = carStorage;
        this.detector = detector;
    }

    @Override
    public void run() {
        while (true) {
            synchronized (carStorage){
                while(carStorage.getFullness() >= 100){
                    try {
                        carStorage.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                synchronized (detector){ detector.notify(); }
            }
        }
    }
    static public class Detector{};

}

