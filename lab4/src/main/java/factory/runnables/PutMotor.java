package factory.runnables;

import factory.Factory;
import factory.Storage;
import factory.objects.Motor;

import java.util.concurrent.TimeUnit;

public class PutMotor implements Runnable {
    private final Storage<Motor> motorStorage;

    public PutMotor(Storage<Motor> motorStorage) {
        this.motorStorage = motorStorage;
    }

    @Override
    public void run() {
        while (true) {
            try {
                TimeUnit.SECONDS.sleep(Factory.dynamicSettings.MOTOR_DEALER_TIME().getSeconds());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            //System.out.println("Кладу в склад двигателей");
            motorStorage.put(new Motor((int) (Math.random() * 1000)));
        }
    }
}
