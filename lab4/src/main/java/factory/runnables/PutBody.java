package factory.runnables;

import factory.Factory;
import factory.Storage;
import factory.objects.Body;

import java.util.concurrent.TimeUnit;

public class PutBody implements Runnable {
    private final Storage<Body> bodyStorage;

    public PutBody(Storage<Body> bodyStorage) {
        this.bodyStorage = bodyStorage;
    }

    @Override
    public void run() {
        while (true) {
            try {
                TimeUnit.SECONDS.sleep(Factory.dynamicSettings.BODY_DEALER_TIME().getSeconds());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            //System.out.println("Кладу в склад кузовов");
            bodyStorage.put(new Body((int) (Math.random() * 1000)));
        }
    }
}
