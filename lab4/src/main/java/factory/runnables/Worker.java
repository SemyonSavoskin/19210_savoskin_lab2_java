package factory.runnables;

import factory.Factory;
import factory.Storage;
import factory.objects.Accessories;
import factory.objects.Body;
import factory.objects.Car;
import factory.objects.Motor;

import java.util.concurrent.TimeUnit;

public class Worker implements Runnable {
    private final Storage<Accessories> accessoriesStorage;
    private final Storage<Body> bodyStorage;
    private final Storage<Motor> motorStorage;
    private final Storage<Car> carStorage;
    private final Object detector;


    public Worker(Storage<Accessories> accessoriesStorage, Storage<Body> bodyStorage,
                  Storage<Motor> motorStorage, Storage<Car> carStorage, Object detector) {
        this.bodyStorage = bodyStorage;
        this.carStorage = carStorage;
        this.motorStorage = motorStorage;
        this.accessoriesStorage = accessoriesStorage;
        this.detector = detector;
    }

    @Override
    public void run() {
        while (true) {
            synchronized (detector){
                try {
                    detector.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            Body body = bodyStorage.get();
            Motor motor = motorStorage.get();
            Accessories accessories = accessoriesStorage.get();
            try {
                TimeUnit.SECONDS.sleep(Factory.dynamicSettings.WORKER_TIME().getSeconds());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Car car = new Car((int) (Math.random()*1000), body, motor, accessories);
            Factory.logger.info("Worker " + Thread.currentThread().getId() + " собрал автомобиль: " + car.CID);
            carStorage.put(car);
        }
    }
}

