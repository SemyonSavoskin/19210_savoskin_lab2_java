package factory.objects;

public class Car {
    public Car(int CID, Body body, Motor motor, Accessories accessories) {
        this.CID = CID;
        this.body = body;
        this.motor = motor;
        this.accessories = accessories;
    }
    public final Body body;
    public final Motor motor;
    public final Accessories accessories;
    public final int CID;
}
