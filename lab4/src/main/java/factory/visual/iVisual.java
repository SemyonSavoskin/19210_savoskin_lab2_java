package factory.visual;

import java.time.Duration;

public interface iVisual {
    Duration getBODY_DEALER_TIME();
    Duration getMOTOR_DEALER_TIME();
    Duration getACCESSORIES_DEALER_TIME();
    Duration getCAR_DEALER_TIME();
    Duration getWORKER_TIME();

    void setFullnessBodyStorage(int size, int capacity);
    void setFullnessMotorStorage(int size, int capacity);
    void setFullnessAccessoriesStorage(int size, int capacity);
    void setFullnessCarStorage(int size, int capacity);
}
