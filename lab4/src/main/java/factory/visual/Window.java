package factory.visual;

import factory.control.VisualOperator;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.time.Duration;

public class Window extends JFrame implements iVisual {
    private Duration BODY_DEALER_TIME;
    private Duration MOTOR_DEALER_TIME;

    private final JProgressBar accessoriesProgBar = new JProgressBar(0,100);
    private final JProgressBar motorProgBar = new JProgressBar(0,100);
    private final JProgressBar bodyProgBar = new JProgressBar(0,100);
    private final JProgressBar carProgBar = new JProgressBar(0,100);

    JLabel accessories = new JLabel("accessories");
    JLabel motor = new JLabel("motor");
    JLabel body = new JLabel("body");
    JLabel mainStorage = new JLabel("car storage");

    @Override
    public void setFullnessBodyStorage(int size, int capacity) {
        bodyProgBar.setValue((int)((float)size/capacity*100));
        String output = String.format("body %d/%d", size, capacity);
        body.setText(output);
    }

    @Override
    public void setFullnessMotorStorage(int size, int capacity) {
        motorProgBar.setValue((int)((float)size/capacity*100));
        String output = String.format("motor %d/%d", size, capacity);
        motor.setText(output);
    }

    @Override
    public void setFullnessAccessoriesStorage(int size, int capacity) {
        accessoriesProgBar.setValue((int)((float)size/capacity*100));
        String output = String.format("accessories %d/%d", size, capacity);
        accessories.setText(output);
    }

    @Override
    public void setFullnessCarStorage(int size, int capacity) {
        carProgBar.setValue((int)((float)size/capacity*100));
        String output = String.format("car storage %d/%d", size, capacity);
        mainStorage.setText(output);
    }

    private Duration ACCESSORIES_DEALER_TIME;
    private Duration CAR_DEALER_TIME;
    private Duration WORKER_TIME;
    public Window(){
        setTitle("Factory");

        BODY_DEALER_TIME = Duration.ofSeconds(VisualOperator.getBODY_DEALER_TIME_DEFAULT());
        MOTOR_DEALER_TIME = Duration.ofSeconds(VisualOperator.getMOTOR_DEALER_TIME_DEFAULT());
        ACCESSORIES_DEALER_TIME = Duration.ofSeconds(VisualOperator.getACCESSORIES_DEALER_TIME_DEFAULT());
        CAR_DEALER_TIME = Duration.ofSeconds(VisualOperator.getCAR_DEALER_TIME_DEFAULT());
        WORKER_TIME = Duration.ofSeconds(VisualOperator.getWORKER_TIME_DEFAULT());

        setDefaultCloseOperation(EXIT_ON_CLOSE);

        JSlider motorslider = new JSlider(0, 10);
        motorslider.setPaintLabels(true);
        motorslider.setMajorTickSpacing(1);

        JSlider bodyslider = new JSlider(0, 10);
        bodyslider.setPaintLabels(true);
        bodyslider.setMajorTickSpacing(1);

        JSlider accessoriesslider = new JSlider(0, 10);
        accessoriesslider.setPaintLabels(true);
        accessoriesslider.setMajorTickSpacing(1);

        JSlider workerslider = new JSlider(0, 20);
        workerslider.setPaintLabels(true);
        workerslider.setMajorTickSpacing(2);

        JSlider dealerslider = new JSlider(0, 20);
        dealerslider.setPaintLabels(true);
        dealerslider.setMajorTickSpacing(2);

        motorslider.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                int value = ((JSlider)e.getSource()).getValue();
                MOTOR_DEALER_TIME = Duration.ofSeconds(value);
            }
        });
        bodyslider.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                int value = ((JSlider)e.getSource()).getValue();
                BODY_DEALER_TIME = Duration.ofSeconds(value);
            }
        });
        accessoriesslider.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                int value = ((JSlider)e.getSource()).getValue();
                ACCESSORIES_DEALER_TIME = Duration.ofSeconds(value);
            }
        });
        workerslider.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                int value = ((JSlider)e.getSource()).getValue();
                WORKER_TIME = Duration.ofSeconds(value);
            }
        });
        dealerslider.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                int value = ((JSlider)e.getSource()).getValue();
                CAR_DEALER_TIME = Duration.ofSeconds(value);
            }
        });

        Font font = new Font("Verdana", Font.PLAIN, 14);

        motor.setFont(font);
        body.setFont(font);
        accessories.setFont(font);
        JLabel workers = new JLabel("workers");
        workers.setFont(font);
        JLabel dealers = new JLabel("dealers");
        dealers.setFont(font);

        JPanel contents = new JPanel(new VerticalLayout());

        motorProgBar.setStringPainted(true);
        carProgBar.setStringPainted(true);
        accessoriesProgBar.setStringPainted(true);
        bodyProgBar.setStringPainted(true);

        contents.add(motor);
        contents.add(motorProgBar);
        contents.add(motorslider);

        contents.add(body);
        contents.add(bodyProgBar);
        contents.add(bodyslider);

        contents.add(accessories);
        contents.add(accessoriesProgBar);
        contents.add(accessoriesslider);

        contents.add(workers);
        contents.add(workerslider);

        contents.add(dealers);
        contents.add(dealerslider);

        mainStorage.setFont(font);
        contents.add(mainStorage);
        contents.add(carProgBar);
        getContentPane().add(contents);

        setSize(250, 520);
        setVisible(true);

    }

    @Override
    public Duration getBODY_DEALER_TIME() {
        return BODY_DEALER_TIME;
    }

    @Override
    public Duration getMOTOR_DEALER_TIME() {
        return MOTOR_DEALER_TIME;
    }

    @Override
    public Duration getACCESSORIES_DEALER_TIME() {
        return ACCESSORIES_DEALER_TIME;
    }

    @Override
    public Duration getCAR_DEALER_TIME() {
        return CAR_DEALER_TIME;
    }

    @Override
    public Duration getWORKER_TIME() {
        return WORKER_TIME;
    }


}

class VerticalLayout implements LayoutManager
{
    private Dimension size = new Dimension();

    // Следующие два метода не используются
    public void addLayoutComponent   (String name, Component comp) {}
    public void removeLayoutComponent(Component comp) {}

    // Метод определения минимального размера для контейнера
    public Dimension minimumLayoutSize(Container c) {
        return calculateBestSize(c);
    }
    // Метод определения предпочтительного размера для контейнера
    public Dimension preferredLayoutSize(Container c) {
        return calculateBestSize(c);
    }
    // Метод расположения компонентов в контейнере
    public void layoutContainer(Container container)
    {
        // Список компонентов
        Component list[] = container.getComponents();
        int currentY = 5;
        for (int i = 0; i < list.length; i++) {
            // Определение предпочтительного размера компонента
            Dimension pref = list[i].getPreferredSize();
            // Размещение компонента на экране
            list[i].setBounds(5, currentY, pref.width, pref.height);
            // Учитываем промежуток в 5 пикселов
            currentY += 5;
            // Смещаем вертикальную позицию компонента
            currentY += pref.height;
        }
    }
    // Метод вычисления оптимального размера контейнера
    private Dimension calculateBestSize(Container c)
    {
        // Вычисление длины контейнера
        Component[] list = c.getComponents();
        int maxWidth = 0;
        for (int i = 0; i < list.length; i++) {
            int width = list[i].getWidth();
            // Поиск компонента с максимальной длиной
            if ( width > maxWidth )
                maxWidth = width;
        }
        // Размер контейнера в длину с учетом левого отступа
        size.width = maxWidth + 5;
        // Вычисление высоты контейнера
        int height = 0;
        for (int i = 0; i < list.length; i++) {
            height += 5;
            height += list[i].getHeight();
        }
        size.height = height;
        return size;
    }
}
