package factory.control;
import java.time.Duration;

public class DynamicSettings {
    public static int DEFAULT_BODY_DEALER_TIME = 5;
    public static int DEFAULT_MOTOR_DEALER_TIME = 5;
    public static int DEFAULT_ACCESSORIES_DEALER_TIME = 5;
    public static int DEFAULT_CAR_DEALER_TIME = 10;
    public static int DEFAULT_WORKER_TIME = 10;
    public DynamicSettings(){
        BODY_DEALER_TIME        = Duration.ofSeconds(DEFAULT_BODY_DEALER_TIME);
        MOTOR_DEALER_TIME       = Duration.ofSeconds(DEFAULT_MOTOR_DEALER_TIME);
        ACCESSORIES_DEALER_TIME = Duration.ofSeconds(DEFAULT_ACCESSORIES_DEALER_TIME);
        CAR_DEALER_TIME         = Duration.ofSeconds(DEFAULT_CAR_DEALER_TIME);
        WORKER_TIME             = Duration.ofSeconds(DEFAULT_WORKER_TIME);
    }

    // скорость работы поставщиков - время ожидания перед итерацией
    private Duration BODY_DEALER_TIME;
    private Duration MOTOR_DEALER_TIME;
    private Duration ACCESSORIES_DEALER_TIME;
    private Duration CAR_DEALER_TIME;
    private Duration WORKER_TIME;

    protected void setBODY_DEALER_TIME(Duration BODY_DEALER_TIME){
        this.BODY_DEALER_TIME = BODY_DEALER_TIME;
    }
    protected void setMOTOR_DEALER_TIME(Duration MOTOR_DEALER_TIME){
        this.MOTOR_DEALER_TIME = MOTOR_DEALER_TIME;
    }
    protected void setACCESSORIES_DEALER_TIME(Duration ACCESSORIES_DEALER_TIME){
        this.ACCESSORIES_DEALER_TIME = ACCESSORIES_DEALER_TIME;
    }
    protected void setCAR_DEALER_TIME(Duration CAR_DEALER_TIME){
        this.CAR_DEALER_TIME = CAR_DEALER_TIME;
    }
    protected void setWORKER_TIME(Duration WORKER_TIME){
        this.WORKER_TIME = WORKER_TIME;
    }

    public Duration BODY_DEALER_TIME        (){ return BODY_DEALER_TIME; }
    public Duration MOTOR_DEALER_TIME       (){ return MOTOR_DEALER_TIME; }
    public Duration ACCESSORIES_DEALER_TIME (){ return ACCESSORIES_DEALER_TIME; }
    public Duration CAR_DEALER_TIME         (){ return CAR_DEALER_TIME; }
    public Duration WORKER_TIME             (){ return WORKER_TIME; }
}

