package factory.control;

import factory.Storage;
import factory.objects.Accessories;
import factory.objects.Body;
import factory.objects.Car;
import factory.objects.Motor;
import factory.visual.iVisual;

import java.time.Duration;

public class VisualOperator {
    // опрашивает визуальный интерфейс в отдельном потоке, настраивает DynamicSettings
    static public void start(iVisual visual, DynamicSettings dynamicSettings,
                             final Storage<Motor> motorStorage, final Storage<Body> bodyStorage,
                             final Storage<Car> carStorage, final Storage<Accessories> accessoriesStorage
    ){
        Thread checker = new Thread(() -> {
            while (true){
                try {
                    Thread.sleep(Duration.ofNanos(500).toNanos());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                dynamicSettings.setACCESSORIES_DEALER_TIME(visual.getACCESSORIES_DEALER_TIME());
                dynamicSettings.setBODY_DEALER_TIME(visual.getBODY_DEALER_TIME());
                dynamicSettings.setWORKER_TIME(visual.getWORKER_TIME());
                dynamicSettings.setMOTOR_DEALER_TIME(visual.getMOTOR_DEALER_TIME());
                dynamicSettings.setCAR_DEALER_TIME(visual.getCAR_DEALER_TIME());

                visual.setFullnessMotorStorage(motorStorage.getFullness(), motorStorage.getCapacity());
                visual.setFullnessBodyStorage(bodyStorage.getSize(), bodyStorage.getCapacity());
                visual.setFullnessCarStorage(carStorage.getFullness(), carStorage.getCapacity());
                visual.setFullnessAccessoriesStorage(accessoriesStorage.getFullness(), accessoriesStorage.getCapacity());

            }
        });
        checker.start();
    }

    public static int getBODY_DEALER_TIME_DEFAULT(){       return DynamicSettings.DEFAULT_BODY_DEALER_TIME;}
    public static int getMOTOR_DEALER_TIME_DEFAULT(){     return DynamicSettings.DEFAULT_MOTOR_DEALER_TIME;}
    public static int getACCESSORIES_DEALER_TIME_DEFAULT(){return DynamicSettings.DEFAULT_ACCESSORIES_DEALER_TIME;}
    public static int getCAR_DEALER_TIME_DEFAULT(){        return DynamicSettings.DEFAULT_CAR_DEALER_TIME;}
    public static int getWORKER_TIME_DEFAULT(){            return DynamicSettings.DEFAULT_WORKER_TIME;}
}
