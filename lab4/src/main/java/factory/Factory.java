package factory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import factory.control.DynamicSettings;
import factory.control.VisualOperator;
import factory.objects.Accessories;
import factory.objects.Body;
import factory.objects.Car;
import factory.objects.Motor;
import factory.runnables.*;
import factory.visual.Window;
import factory.visual.iVisual;
import threadpool.ThreadPool;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class Factory {
    private Storage<Accessories> accessoriesStorage;
    private Storage<Body> bodyStorage;
    private Storage<Motor> motorStorage;
    private Storage<Car> carStorage;

    public static DynamicSettings dynamicSettings = new DynamicSettings();

    public static Logger logger = LoggerFactory.getLogger(Factory.class);

    public void main(String[] args) {
        iVisual Win = new Window();

        //logger.info("Example log from {} ", Factory.class.getSimpleName());

        FileInputStream file = null;
        try {
            file = new FileInputStream("src/main/resources/config.properties");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Properties property = new Properties();
        try {
            property.load(file);
        } catch (IOException exception) {
            exception.printStackTrace();
        }

        ThreadPool accessoriesThreadPool = new ThreadPool(Integer.parseInt(property.getProperty("AccessorySuppliers")));
        ThreadPool bodyThreadPool = new ThreadPool(1);
        ThreadPool motorThreadPool = new ThreadPool(1);
        ThreadPool controllersThreadPool = new ThreadPool(1);
        ThreadPool workersThreadPool = new ThreadPool(Integer.parseInt(property.getProperty("Workers")));
        ThreadPool dealersThreadPool = new ThreadPool(Integer.parseInt(property.getProperty("Dealers")));


        accessoriesStorage = new Storage<>(Integer.parseInt(property.getProperty("StorageAccessorySize")));
        bodyStorage = new Storage<>(Integer.parseInt(property.getProperty("StorageBodySize")));
        motorStorage = new Storage<>(Integer.parseInt(property.getProperty("StorageMotorSize")));
        carStorage = new Storage<>(Integer.parseInt(property.getProperty("StorageAutoSize")));

        Controller.Detector detector = new Controller.Detector();

        VisualOperator.start(Win, dynamicSettings, motorStorage, bodyStorage, carStorage, accessoriesStorage);
        for (int i = 0; i < Integer.parseInt(property.getProperty("AccessorySuppliers")); i++) {
            accessoriesThreadPool.execute(new PutAccessories(accessoriesStorage));
        }
        bodyThreadPool.execute(new PutBody(bodyStorage));
        motorThreadPool.execute(new PutMotor(motorStorage));
        for (int i = 0; i < Integer.parseInt(property.getProperty("Workers")); i++) {
            workersThreadPool.execute(new Worker(accessoriesStorage, bodyStorage, motorStorage, carStorage, detector));
        }
        for (int i = 0; i < Integer.parseInt(property.getProperty("Dealers")); i++) {
            dealersThreadPool.execute(new GetCar(carStorage));
        }
        controllersThreadPool.execute(new Controller(carStorage, detector));

        accessoriesThreadPool.shutdown();
        motorThreadPool.shutdown();
        bodyThreadPool.shutdown();
        workersThreadPool.shutdown();
        dealersThreadPool.shutdown();
        controllersThreadPool.shutdown();
    }

}
